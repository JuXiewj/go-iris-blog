# go-iris-blog

### 介绍
基于  **golang**   **iris**  **vue**  编写的一款  **bbs**   **blog** 论坛 博客

#### 前台
vue element-ui mackdown

#### 后台
golang iris jwt 敏感词过滤

demo: http://www.hugebug.top/index

#### 主页

![输入图片说明](https://images.gitee.com/uploads/images/2020/0908/164541_05a522e8_1538978.png "屏幕截图.png")

#### 注册
![输入图片说明](https://images.gitee.com/uploads/images/2020/0908/164557_8ca2f36e_1538978.png "屏幕截图.png")

#### 编辑
![输入图片说明](https://images.gitee.com/uploads/images/2020/0908/164615_c69a54de_1538978.png "屏幕截图.png")