import Vue from 'vue'
import VueRouter from 'vue-router'
import ArticleDetail from '../views/ArticleDetail.vue'
import ArticleEdit from '../views/ArticleEdit.vue'
import Login from '../views/Login.vue'
import Index from '../views/Index.vue'
import Column from '../views/Column.vue'
import Space from '../views/Space.vue'
import ArticleColumn from '../views/ArticleColumn.vue'
import ColumnEdit from '../views/ColumnEdit.vue'
import Reg from '../views/Reg.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Index',
        component: Index,
        meta: {
            requireAuth: false
        }
    },
    {
        path: '/index',
        name: 'Index',
        component: Index,
        meta: {
            requireAuth: false
        }
    },
    {
        path: '/column',
        name: 'Column',
        component: Column
    },
    {
        path: '/articleColumn',
        name: 'ArticleColumn',
        component: ArticleColumn,
        meta: {
            requireAuth: false
        }
    },
    {
        path: '/columnEdit',
        name: 'ColumnEdit',
        component: ColumnEdit,
        meta: {
            requireAuth: true
        }
    },

    {
        path: '/space',
        name: 'Space',
        component: Space
    },
    {
        path: '/detail',
        name: 'ArticleDetail',
        component: ArticleDetail,
        meta: {
            requireAuth: false
        }
    },
    {
        path: '/edit',
        name: 'ArticleEdit',
        component: ArticleEdit,
        meta: {
            requireAuth: true
        }
    },
    {
        path: '/login',
        name: 'Login',
        component: Login
    },
    {
        path: '/reg',
        name: 'Reg',
        component: Reg
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location) {
    return originalPush.call(this, location).catch(err => err)
}

export default router
