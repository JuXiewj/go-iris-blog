import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Element  from 'element-ui'
import "element-ui/lib/theme-chalk/index.css"
import 'mavon-editor/dist/css/index.css'
import request from "./axios"
import "./permission"

import mavonEditor from 'mavon-editor'

Vue.use(Element)
Vue.use(mavonEditor)

Vue.config.productionTip = false
Vue.prototype.$request = request

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
