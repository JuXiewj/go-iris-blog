package services

import (
	"goblog/daos"
	"goblog/pojo/entity"
)

type UserService struct {
	dao *daos.UserDao
}

func NewUserService(repo *daos.UserDao) *UserService {
	return &UserService{
		dao: repo,
	}
}

func (service *UserService)Add(u *entity.User)bool{
	return service.dao.AddUser(u)
}
func (service *UserService)DeleteById(id int64) bool{
	return  service.dao.DeleteById(id)
}
func (service *UserService)Update(u *entity.User) bool{
	return  service.dao.UpdateUser(u)
}
func (service *UserService)GetAll() []entity.User{
	return  nil
}
func (service *UserService)GetById(id int64) *entity.User{
	return service.dao.GetUserById(id)
}

func (service *UserService) GetByUserName(username string) *entity.User{
	return service.dao.GetByUserName(username)
}
