package services

import (
	"goblog/daos"
	"goblog/pojo/entity"
)

type CommentService struct {
	dao *daos.CommentDao
}

func NewCommentService(repo *daos.CommentDao) *CommentService {
	return &CommentService{
		dao: repo,
	}
}

func (service *CommentService)Add(u *entity.Comment)bool{
	return service.dao.AddComment(u)
}
func (service *CommentService)DeleteById(id int64) bool{
	return  service.dao.DeleteById(id)
}
func (service *CommentService)Update(u *entity.Comment) bool{
	return  service.dao.UpdateComment(u)
}
func (service *CommentService)GetAll() []entity.Comment{
	return  nil
}
func (service *CommentService)GetById(id int64) *entity.Comment{
	return service.dao.GetCommentById(id)
}

func (service *CommentService) GetByArticleId(articleId int64) []entity.Comment {
	return service.dao.GetByArticleId(articleId)
}

