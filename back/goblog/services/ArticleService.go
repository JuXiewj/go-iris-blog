package services

import (
	"goblog/daos"
	"goblog/pojo/entity"
)

type ArticleService struct {
	dao *daos.ArticleDao
}

func NewArticleService(repo *daos.ArticleDao) *ArticleService {
	return &ArticleService{
		dao: repo,
	}
}

func (service *ArticleService)Add(u *entity.Article)bool{
	return service.dao.AddArticle(u)
}
func (service *ArticleService)DeleteById(id int64) bool{
	return  service.dao.DelById(id)
}
func (service *ArticleService)Update(u entity.Article) bool{
	return  service.dao.UpdateArticle(u)
}
func (service *ArticleService)GetAll() []entity.Article{

	return  nil
}
func (service *ArticleService)GetById(id int64) *entity.Article{
	return service.dao.GetArticleById(id)
}

func (service *ArticleService) FindPage(size int, pageNum int,article  *entity.Article)([]entity.Article,int64) {

	return service.dao.GetArticlePage(size*pageNum,size,article)
}

func (service *ArticleService) FindTop(size int) []entity.Article {
	return service.dao.FindTop(size)
}

func (service *ArticleService) FindAuthorTop(userId int64, size int) []entity.Article {
	return service.dao.FindAuthorTop(userId,size)
}