package entity

import "time"

type Column struct {
	Id int64 `xorm:"pk autoincr" json:"id"`
	UserId  int64  	     `xorm:"BigInt notnull unique 'user_id'" json:"userId"`
	Title   string       `xorm:"varchar(30) notnull 'title'" json:"title"`
	TitleImg string      `xorm:"varchar(50)  'title_img'" json:"titleImg"`
	VisitNum   int       `xorm:"Int 'visit_num'" json:"visitNum"`
	CommentNum   int     `xorm:"Int 'comment_num'" json:"commentNum"`
	PraiseNum  int       `xorm:"Int 'praise_num'" json:"praiseNum"`
	CreateTime time.Time `xorm:"created 'create_time'" json:"createTime"`
	UpdateTime time.Time `xorm:"updated 'update_time'" json:"updateTime"`
	Version int64        `xorm:"version" json:"version"`
}
