package entity

import "time"

type User struct {
	Id int64 `xorm:"pk autoincr" json:"id"`
	NickName   string `xorm:"varchar(50) 'nick_name'" json:"nickName"`
	Avatar string `xorm:"varchar(100) notnull 'avatar'" json:"avatar"`
	UserName string  `xorm:"varchar(50) notnull unique 'user_name'" json:"userName"`
	Password    string `xorm:"varchar(100) notnull 'password'" json:"-"`
	Role   int    `xorm:"default 1" json:"role"` // 0 管理员 1正常用户
	CreateTime time.Time	 `xorm:"created" json:"createTime"`
	UpdateTime time.Time `xorm:"updated" json:"updateTime"`
	Version int64 `xorm:"version" json:"version"`
}