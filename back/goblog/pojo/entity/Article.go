package entity

import "time"

type Article struct {
	Id int64             `xorm:"pk autoincr" json:"id"`
	UserId  int64  	     `xorm:"BigInt notnull 'user_id'" json:"userId"`
	UserName  string  	 `xorm:"varchar(30) notnull 'user_name'" json:"userName"`
	ColumnId  int64  	 `xorm:"BigInt 'column_id'" json:"columnId"`
	Title   string       `xorm:"varchar(30) notnull 'title'" json:"title"`
	Description string   `xorm:"varchar(300) notnull 'description'" json:"description"`
	Content string       `xorm:"text  'content'" json:"content"`
	Source  string       `xorm:"varchar(100)  'source'" json:"source"`
	Gps string           `xorm:"varchar(50)  'gps'" json:"gps"`
	TitleImg string      `xorm:"varchar(50)  'title_img'" json:"titleImg"`
	VisitNum   int       `xorm:"Int 'visit_num'" json:"visitNum"`
	CommentNum   int     `xorm:"Int 'comment_num'" json:"commentNum"`
	PraiseNum  int       `xorm:"Int 'praise_num'" json:"praiseNum"`
	CreateTime time.Time `xorm:"created 'create_time'" json:"createTime"`
	UpdateTime time.Time `xorm:"updated 'update_time'" json:"updateTime"`
	Version int64        `xorm:"version" json:"version"`
}
