package entity

import "time"
/**
 评论
 */
type Comment struct {
	Id int64             `xorm:"pk autoincr" json:"id"`
	UserId  int64  	     `xorm:"BigInt notnull 'user_id'" json:"userId"`
	UserName  string  	 `xorm:"varchar(30) 'user_name'" json:"userName"`
	UserAvatar  string   `xorm:"varchar(200) 'user_avatar'" json:"userAvatar"`
	ArticleId int64      `xorm:"BigInt 'article_id'" json:"articleId"`
	Content string       `xorm:"varchar(500)  'content'" json:"content"`
	PraiseNum  int       `xorm:"Int 'praise_num'" json:"praiseNum"`
	Type int             `xorm:"Int 'type'" json:"type"`
	ReplyId int64        `xorm:"BigInt 'reply_id'" json:"replyId"`
	ReplyUserId int64    `xorm:"BigInt 'reply_user_id'" json:"replyUserId"`
	ReplyUserName string `xorm:"varchar(30) 'reply_user_name'" json:"replyUserName"`
	CreateTime time.Time `xorm:"created 'create_time'" json:"createTime"`
	UpdateTime time.Time `xorm:"updated 'update_time'" json:"updateTime"`
	Version int64        `xorm:"version" json:"version"`
}
