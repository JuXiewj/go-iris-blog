package dto

import "time"
/**
 评论
 */
type CommentDto struct {
	Id int64               `json:"id"`
	UserId  int64  	       `json:"userId"`
	UserName  string  	   `json:"userName"`
	UserAvatar string      `json:"userAvatar"`
	ArticleId int64        `json:"articleId"`
	Content string         `json:"content"`
	PraiseNum  int         `json:"praiseNum"`
	Type int               `json:"type"`
	ReplyId int64          `json:"replyId"`
	ReplyUserId int64      `json:"replyUserId"`
	ReplyUserName string   `json:"replyUserName"`
	CreateTime time.Time   `json:"createTime"`
	UpdateTime time.Time   `json:"updateTime"`
	Version int64          `json:"version"`
	Children []CommentDto  `json:"children"`
}
