package daos

import (
	"goblog/pojo/entity"
	"goblog/tools"
)

type CommentDao struct {
	*tools.Orm
}

func NewCommentDao(engine *tools.Orm) *CommentDao {
	return &CommentDao{
		engine,
	}
}

func (dao *CommentDao)AddComment(u *entity.Comment)bool{
	_,err:=dao.InsertOne(u)
	if err!=nil{
		dao.Logger().Error(err)
		return false
	}
	return true
}

func (dao *CommentDao)DeleteById(id int64)bool{
	comment := entity.Comment{Id: id}
	if ok, err := dao.Get(&comment); ok {
		dao.Logger().Error(err)
		return true
	}
	return false
}

func (dao *CommentDao)UpdateComment(comment *entity.Comment)bool{
	_,err:=dao.Update(comment)
	if err!=nil{
		dao.Logger().Error(err)
		return false
	}
	return true
}

func (dao *CommentDao)GetCommentById(id int64) *entity.Comment{
	var comment entity.Comment
	comment = entity.Comment{Id:id}
	exist, err := dao.Get(&comment)
	if err!=nil {
		dao.Logger().Error(err)
		return nil
	}
	if !exist {
		return nil
	}
	return &comment
}

func (dao *CommentDao) GetByArticleId(articleId int64) []entity.Comment {
	var comment entity.Comment
	comment = entity.Comment{ArticleId:articleId}
	var commList []entity.Comment = make([]entity.Comment, 0)
	err := dao.Find(&commList,&comment)
	if err!=nil {
		dao.Logger().Error(err)
		return nil
	}
	return commList
}

