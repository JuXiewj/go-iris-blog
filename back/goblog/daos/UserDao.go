package daos

import (
	"goblog/pojo/entity"
	"goblog/tools"
)

type UserDao struct {
	*tools.Orm
}

func NewUserDao(engine *tools.Orm) *UserDao {
	return &UserDao{
		engine,
	}
}

func (dao *UserDao)AddUser(u *entity.User)bool{
	_,err:=dao.InsertOne(u)
	if err!=nil{
		dao.Logger().Error(err)
		return false
	}
	return true
}

func (dao *UserDao)DeleteById(id int64)bool{
	user := entity.User{Id: id}
	if ok, err := dao.Get(&user); ok {
		dao.Logger().Error(err)
		return true
	}
	return false
}

func (dao *UserDao)UpdateUser(user *entity.User)bool{
	_,err:=dao.Update(user)
	if err!=nil{
		dao.Logger().Error(err)
		return false
	}
	return true
}

func (dao *UserDao)GetUserById(id int64) *entity.User{
	var user entity.User
	user = entity.User{Id:id}
	exist, err := dao.Get(&user)
	if err!=nil {
		dao.Logger().Error(err)
		return nil
	}
	if !exist {
		return nil
	}
	return &user
}

func (dao *UserDao) GetByUserName(username string) *entity.User {
	var user entity.User
	user = entity.User{UserName:username}
	exist, err := dao.Get(&user)
	if err!=nil {
		dao.Logger().Error(err)
		return nil
	}
	if !exist {
		return nil
	}
	return &user
}