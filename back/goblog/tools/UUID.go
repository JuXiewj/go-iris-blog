package tools

import (
	"github.com/satori/go.uuid"
	"strings"
)

func Uuid() string{
	u := uuid.NewV4()
	return u.String()
}

func UuidSimple() string{
	u := uuid.NewV4()
	return strings.ReplaceAll(u.String(),"-","")
}