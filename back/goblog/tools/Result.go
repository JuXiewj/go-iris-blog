package tools

import (
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
)

func ResultSuccess(data interface{}) mvc.Result{
	return mvc.Response{
		Code: iris.StatusOK,
		Object:map[string]interface{}{
			"code":200,
			"msg":"success",
			"data":data,
		},
	}
}

func ResultFail() mvc.Result{
	return mvc.Response{
		Code: iris.StatusInternalServerError,
		Object:map[string]interface{}{
			"code":500,
			"msg":"fail",
		},
	}
}

func ResultSuccessMsg(msg string) mvc.Result{
	return mvc.Response{
		Code: iris.StatusOK,
		Object:map[string]interface{}{
			"code":200,
			"msg":msg,
		},
	}
}

func ResultFailMsg(msg string) mvc.Result{
	return mvc.Response{
		Code: iris.StatusInternalServerError,
		Object:map[string]interface{}{
			"code":500,
			"msg":msg,
		},
	}
}