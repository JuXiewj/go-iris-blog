package tools

import (
	"golang.org/x/crypto/bcrypt"
)

func Pwd(arg string) (string,error){
	hash, err := bcrypt.GenerateFromPassword([]byte(arg), bcrypt.DefaultCost)
	return string(hash),err
}

func PwdCheck(p string, m string) bool{
	err := bcrypt.CompareHashAndPassword([]byte(m), []byte(p))
	if err != nil {
		return false
	}
	return true
}

