package tools

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/go-xorm/xorm"
	"goblog/config"
	"goblog/pojo/entity"
)

var DbEngine *Orm

type Orm struct {
	*xorm.Engine
}

func OrmEngine(cfg *config.Config) (*Orm, error) {
	database := cfg.Database
	conn := database.User + ":" + database.Password + "@tcp(" + database.Host + ":" + database.Port + ")/" + database.DbName + "?charset=" + database.Charset
	engine, err := xorm.NewEngine(database.Driver, conn)
	if err != nil {
		return nil, err
	}

	engine.ShowSQL(database.ShowSql)

	err = engine.Sync2(
		new(entity.Column),
		new(entity.Comment),
		new(entity.Article),
		new(entity.User))

	if err != nil {
		return nil, err
	}

	orm := new(Orm)
	orm.Engine = engine
	DbEngine = orm

	return orm, nil
}