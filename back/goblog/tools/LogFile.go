package tools

import (
	"os"
	"time"
)

// 按天生成日志文件
func TodayFilename() string {
	today := time.Now().Format("20060102")
	return today + ".log"
}

// 创建打开文件
func NewLogFile() *os.File {
	filename := TodayFilename()
	file, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}
	return file
}