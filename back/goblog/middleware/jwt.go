package middleware

import (
	"goblog/myjwt"
)

/**
 * 验证 jwt
 * @method JwtHandler
 */
func JwtHandler() *myjwt.Middleware {
	var mySecret = []byte("123")
	return myjwt.New(myjwt.Config{
		//这个方法将验证jwt的token
		ValidationKeyGetter: func(token *myjwt.Token) (interface{}, error) {
			//自己加密的秘钥或者说盐值
			return mySecret, nil
		},
		//设置后，中间件会验证令牌是否使用特定的签名算法进行签名
		//如果签名方法不是常量，则可以使用ValidationKeyGetter回调来实现其他检查
		//重要的是要避免此处的安全问题：https://auth0.com/blog/2015/03/31/critical-vulnerabilities-in-json-web-token-libraries/
		//加密的方式
		SigningMethod: myjwt.SigningMethodHS256,
		//验证未通过错误处理方式
		//ErrorHandler: func(context.Context, string)

		//debug 模式
		//Debug: bool
		Ignores: []string{
			"/index",
			"/user/upload/avg",
			"/user/login",
			"/user/info",
			"/user/detail",
			"/user/reg",
			"/index/article/page",
			"/space/article/page",
			"/article/info",
			"/article/top",
			"/article/visit",
			"/comment/list"},
	})
}