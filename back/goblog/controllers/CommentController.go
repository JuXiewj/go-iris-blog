package controllers

import (
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
	"github.com/kataras/iris/v12/sessions"
	"goblog/myjwt"
	"goblog/pojo/dto"
	"goblog/pojo/entity"
	"goblog/services"
	"goblog/tools"
	"strconv"
)

type CommentController struct{
	//上下文对象
	Ctx iris.Context
	//user service
	CommentService *services.CommentService
	//session对象
	Session *sessions.Session
}

func (comm *CommentController) PostAdd()  mvc.Result{
	content := comm.Ctx.FormValue("content")
	articleIdF := comm.Ctx.FormValue("articleId")
	articleId,_ := strconv.ParseInt(articleIdF,10,64)
	user,err := myjwt.ParseJwtUser(comm.Ctx)
	if err !=nil{
		return tools.ResultFailMsg("jwt解析失败!")
	}

	content = tools.MgcReplace(content)
	if len(content) == 0{
		return tools.ResultFailMsg("敏感词过滤后,内荣为空!")
	}
	comment :=  entity.Comment{
				Content:content,
				ArticleId:articleId,
				UserId: user.Id,
		        UserName: user.UserName,
		        UserAvatar: user.Avatar,
				Type: 1,
			}
	res := comm.CommentService.Add(&comment)
	if res {
		return tools.ResultSuccessMsg("操作成功")
	}else{
		return tools.ResultFailMsg("操作失败")
	}
}

func (comm *CommentController) PostRep() mvc.Result{
	content := comm.Ctx.FormValue("content")
	articleIdF := comm.Ctx.FormValue("articleId")
	articleId,_ := strconv.ParseInt(articleIdF,10,64)
	replyIdF := comm.Ctx.FormValue("replyId")
	replyId,_ := strconv.ParseInt(replyIdF,10,64)
	replyUserIdF := comm.Ctx.FormValue("replyUserId")
	replyUserId,_ := strconv.ParseInt(replyUserIdF,10,64)
	replyUserName := comm.Ctx.FormValue("replyUserName")

	content = tools.MgcReplace(content)
	if len(content) == 0{
		return tools.ResultFailMsg("敏感词过滤后,内荣为空!")
	}
	user,err := myjwt.ParseJwtUser(comm.Ctx)
	if err !=nil{
		return tools.ResultFailMsg("jwt 解析失败")
	}
	comment :=  entity.Comment{
		Content:content,
		ArticleId:articleId,
		UserId: user.Id,
		UserName: user.UserName,
		UserAvatar: user.Avatar,
		ReplyId:replyId,
		ReplyUserId:replyUserId,
		ReplyUserName:replyUserName,
		Type: 2,
	}
	res := comm.CommentService.Add(&comment)
	if res {
		return tools.ResultSuccessMsg("操作成功")
	}else{
		return tools.ResultSuccessMsg("操作失败")
	}
}

func (comm *CommentController) PostList()  mvc.Result{
	articleIdF := comm.Ctx.FormValue("articleId")
	articleId,_ := strconv.ParseInt(articleIdF,10,64)
	comments := comm.CommentService.GetByArticleId(articleId)
	return tools.ResultSuccess(reset(comments))
}


func reset(comments []entity.Comment)[]dto.CommentDto{
	var list []dto.CommentDto = make([]dto.CommentDto,0)
	for _, comm := range comments {
		commDto := toDto(&comm)
		if commDto.Type == 1 {
			recursionFn(comments, &commDto)
			list = append(list, commDto)
		}
	}
	return list
}

func toDto(entity *entity.Comment)dto.CommentDto{
	var dto dto.CommentDto = dto.CommentDto{
		Id:            entity.Id,
		UserId:        entity.UserId,
		UserName:      entity.UserName,
		UserAvatar:    entity.UserAvatar,
		ArticleId:     entity.ArticleId,
		Content:       entity.Content,
		PraiseNum:     entity.PraiseNum,
		Type:          entity.Type,
		ReplyId:       entity.ReplyId,
		ReplyUserId:   entity.ReplyUserId,
		ReplyUserName: entity.ReplyUserName,
		CreateTime:    entity.CreateTime,
		UpdateTime:    entity.UpdateTime,
		Version:       entity.Version,
		Children:      nil,
	}
	return dto
}

func recursionFn(comments []entity.Comment,t *dto.CommentDto){
	childList := getChildList(comments, t)
	t.Children = childList
	for _, comm := range childList {
		if hasChild(comments,&comm) {
			for _, ccomm := range childList {
				recursionFn(comments,&ccomm)
			}
		}
	}
}

func getChildList(comments []entity.Comment,t *dto.CommentDto)[]dto.CommentDto{
	var list []dto.CommentDto = make([]dto.CommentDto,0)
	for _, comm := range comments {
		commDto := toDto(&comm)
		if commDto.ReplyId == t.Id {
			recursionFn(comments, &commDto)
			list = append(list, commDto)
		}
	}
	return list
}

func  hasChild(comments []entity.Comment, tChild *dto.CommentDto)bool{
	if len(getChildList(comments, tChild))>0 {
		return  true
	}else{
		return false
	}
}