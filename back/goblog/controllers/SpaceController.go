package controllers

import (
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
	"github.com/kataras/iris/v12/sessions"
	"goblog/pojo/entity"
	"goblog/services"
	"goblog/tools"
	"strconv"
)

type SpaceController struct{
	//上下文对象
	Ctx iris.Context
	//user service
	UserService *services.UserService
	ArticleService *services.ArticleService
	//session对象
	Session *sessions.Session
}

func (sc *SpaceController) PostUseInfo() mvc.Result {
	userId,_ := sc.Ctx.PostValueInt64("userId")
	user := sc.UserService.GetById(userId)
	return tools.ResultSuccess(user)
}

func (sc *SpaceController) PostArticlePage() mvc.Result {
	userIdP:= sc.Ctx.FormValue("userId")
	sizep:= sc.Ctx.FormValue("size")
	pageNumP:= sc.Ctx.FormValue("pageNum")

	userId,_ := strconv.ParseInt(userIdP,10,64)
	size,_ := strconv.Atoi(sizep)
	pageNum,_ := strconv.Atoi(pageNumP)
	article := entity.Article{
		UserId:userId,
	}
	articles,total := sc.ArticleService.FindPage(size,pageNum,&article)
	return tools.ResultSuccess(map[string]interface{}{
		"articles":articles,
		"total":total,
	})
}