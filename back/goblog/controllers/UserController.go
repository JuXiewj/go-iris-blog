package controllers

import (
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
	"github.com/kataras/iris/v12/sessions"
	"goblog/myjwt"
	"goblog/pojo/entity"
	"goblog/services"
	"goblog/tools"
	"strconv"
	"strings"
	"time"
)

type UserController struct{
	//上下文对象
	Ctx iris.Context
	//user service
	UserService *services.UserService
	//session对象
	Session *sessions.Session
}

func (uc *UserController) PostReg() mvc.Result {

	nickName := uc.Ctx.FormValue("nickName")
	avatar := uc.Ctx.FormValue("avatar")
	username := uc.Ctx.FormValue("username")
	password := uc.Ctx.FormValue("password")

	if len(username) == 0{
        return tools.ResultFailMsg("username 为空")
	}
	if len(password) == 0{
		return tools.ResultFailMsg("password 为空")
	}
	exist := uc.UserService.GetByUserName(username)
	if exist != nil {
		return tools.ResultFailMsg("用户已存在")
	}
	pwd,err := tools.Pwd(password)
	if err != nil{

	}
	user := entity.User{
		NickName:   nickName,
		Avatar:     avatar,
		UserName:   username,
		Password:   pwd,
		Role:       1,
	}
	success := uc.UserService.Add(&user)
	if !success {
		return tools.ResultFailMsg("保存失败")
	}
	token := myjwt.NewTokenWithClaims(myjwt.SigningMethodHS256, myjwt.MapClaims{
		"id": strconv.FormatInt(user.Id, 10),
		"username": user.UserName,
		"avatar": user.Avatar,
		"exp": time.Now().Add(time.Hour * time.Duration(1)).Unix(),
		"iat": time.Now().Unix(),
	})
	tokedStr,_:= token.SignedString([]byte("123"))

	return tools.ResultSuccess(map[string]interface{}{
		"token": tokedStr,
		"user":  user,
	})
}


func (uc *UserController) PostLogin() mvc.Result {

	username := uc.Ctx.FormValue("username")
	password := uc.Ctx.FormValue("password")

	if len(username) == 0{
		return tools.ResultFailMsg("username 为空")
	}
	if len(password) == 0{
		return tools.ResultFailMsg("password 为空")
	}

	user := uc.UserService.GetByUserName(username)

	if user == nil {
		return tools.ResultFailMsg("用户不存在")
	}

	success := tools.PwdCheck(password,user.Password)
	if !success {
		return tools.ResultFailMsg("密码不正确")
	}

	token := myjwt.NewTokenWithClaims(myjwt.SigningMethodHS256, myjwt.MapClaims{
		"id": strconv.FormatInt(user.Id, 10),
		"username": user.UserName,
		"avatar": user.Avatar,
		"exp": time.Now().Add(time.Hour * time.Duration(1)).Unix(),
		"iat": time.Now().Unix(),
	})
	tokedStr,_:= token.SignedString([]byte("123"))
	return tools.ResultSuccess(map[string]interface{}{
		"token": tokedStr,
		"user":  user,
	})
}

func (uc *UserController) PostInfo() mvc.Result {
	username := uc.Ctx.FormValue("username")

	if len(username) == 0{
		return tools.ResultFailMsg("username 为空")
	}

	user := uc.UserService.GetByUserName(username)
	if user == nil {
		return tools.ResultFailMsg("用户不存在")
	}
	return tools.ResultSuccess(user)
}

func (uc *UserController) PostUploadAvg() mvc.Result{

	file, info, err := uc.Ctx.FormFile("img")
	if err != nil {
		return mvc.Response{
			Code:iris.StatusInternalServerError,
		}
	}
	defer file.Close()

	arr := strings.Split(info.Filename,".")
	if len(arr) == 2{
		info.Filename = tools.UuidSimple()+"."+arr[1]
	}
	_,err = uc.Ctx.UploadFormFiles("/srv/upload/")
	if err != nil {
		return tools.ResultFailMsg("上传失败")
	}

	return tools.ResultSuccess(map[string]interface{}{
		"imgUrl":"http://47.105.68.156:10000/"+info.Filename,
	})
}

func (uc *UserController) PostDetail() mvc.Result {
	userIdF:= uc.Ctx.FormValue("userId")
	if len(userIdF) == 0{
		return tools.ResultFailMsg("userId 参数为空")
	}
	userId,_ := strconv.ParseInt(userIdF,10,64)
	user := uc.UserService.GetById(userId)
	if user == nil {
		return tools.ResultFailMsg("用户不存在")
	}
	return tools.ResultSuccess(user)
}
