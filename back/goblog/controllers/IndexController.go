package controllers

import (
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
	"github.com/kataras/iris/v12/sessions"
	"goblog/pojo/entity"
	"goblog/services"
	"goblog/tools"
	"strconv"
)

type IndexController struct{
	//上下文对象
	Ctx iris.Context
	//user service
	UserService *services.UserService
	ArticleService *services.ArticleService
	//session对象
	Session *sessions.Session
}

func (ic *IndexController) PostArticlePage() mvc.Result {
	title:= ic.Ctx.FormValue("title")
	sizep:= ic.Ctx.FormValue("size")
	pageNumP:= ic.Ctx.FormValue("pageNum")

	size,_ := strconv.Atoi(sizep)
	pageNum,_ := strconv.Atoi(pageNumP)
	article := entity.Article{
		Title:title,
	}
	articles,total := ic.ArticleService.FindPage(size,pageNum,&article)
	return tools.ResultSuccess(map[string]interface{}{
		"articles":articles,
		"total":total,
	})
}

func (ic *IndexController) Get() mvc.Result {
	return tools.ResultSuccess(map[string]interface{}{
		"index":"blog",
	})
}
