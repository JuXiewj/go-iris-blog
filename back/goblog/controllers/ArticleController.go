package controllers

import (
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
	"github.com/kataras/iris/v12/sessions"
	"goblog/myjwt"
	"goblog/pojo/entity"
	"goblog/services"
	"goblog/tools"
	"strconv"
	"strings"
)

type ArticleController struct{
	//上下文对象
	Ctx iris.Context
	//service
	ArticleService *services.ArticleService
	//user service
	UserService *services.UserService
	//session对象
	Session *sessions.Session
}

func (ac *ArticleController) PostUploadImg() mvc.Result{

	file, info, err := ac.Ctx.FormFile("img")
	if err != nil {
		return mvc.Response{
			Code:iris.StatusInternalServerError,
		}
	}
	defer file.Close()

	arr := strings.Split(info.Filename,".")
	if len(arr) == 2{
		info.Filename = tools.UuidSimple()+"."+arr[1]
	}
	_,err = ac.Ctx.UploadFormFiles("/srv/upload/")
	if err != nil {
		return tools.ResultFailMsg("上传失败")
	}

	return tools.ResultSuccess(map[string]interface{}{
		"imgUrl":"http://47.105.68.156:10000/"+info.Filename,
	})
}

func (ac *ArticleController) PostEdit() mvc.Result {
	user ,err := myjwt.ParseJwtUser(ac.Ctx)
	if err != nil {
		return tools.ResultFailMsg("token 解析失败")
	}

	title := ac.Ctx.FormValue("title")
	description := ac.Ctx.FormValue("description")
	content := ac.Ctx.FormValue("content")

	gps := tools.FindGp(content)

	content = tools.MgcReplace(content)
	if len(content) == 0{
		return tools.ResultFailMsg("敏感词过滤后,内荣为空!")
	}

	article := entity.Article{
		Title:title,
		UserId:user.Id,
		UserName:user.UserName,
		Description:description,
		Content:content,
		Gps:strings.Join(gps,","),
	}
	success := ac.ArticleService.Add(&article)
	if success{
		return tools.ResultSuccessMsg("保存成功")
	}
	return tools.ResultFailMsg("保存失败")
}

func (ac *ArticleController) PostInfo() mvc.Result {
	idStr:= ac.Ctx.FormValue("id")
	if len(idStr) == 0{
		return tools.ResultFailMsg("id 参数为空")
	}
	id,_ := strconv.ParseInt(idStr,10,64)
	article := ac.ArticleService.GetById(id)
	if article == nil {
		return tools.ResultFailMsg("找不到相关文章")
	}
	return tools.ResultSuccess(article)
}


func (ac *ArticleController) PostTop() mvc.Result {
	sizeF:= ac.Ctx.FormValue("size")
	size,_ := strconv.Atoi(sizeF)
	articleList := ac.ArticleService.FindTop(size)
	return tools.ResultSuccess(articleList)
}

func (ac *ArticleController) PostVisit() mvc.Result {
	idStr:= ac.Ctx.FormValue("id")
	if len(idStr) == 0{
		return tools.ResultFailMsg("id 参数为空")
	}
	id,_ := strconv.ParseInt(idStr,10,64)
	article := ac.ArticleService.GetById(id)
	article.VisitNum = article.VisitNum+1
	ac.ArticleService.Update(*article)
	return tools.ResultSuccess(nil)
}

func (ac *ArticleController) PostComment() mvc.Result {
	idStr:= ac.Ctx.FormValue("id")
	if len(idStr) == 0{
		return tools.ResultFailMsg("id 参数为空")
	}
	id,_ := strconv.ParseInt(idStr,10,64)
	article := ac.ArticleService.GetById(id)
	article.CommentNum = article.CommentNum+1
	ac.ArticleService.Update(*article)
	return tools.ResultSuccess(nil)
}

func (ac *ArticleController) PostPraise() mvc.Result {
	idStr:= ac.Ctx.FormValue("id")
	if len(idStr) == 0{
		return tools.ResultFailMsg("id 参数为空")
	}
	id,_ := strconv.ParseInt(idStr,10,64)
	article := ac.ArticleService.GetById(id)
	article.PraiseNum = article.PraiseNum+1
	ac.ArticleService.Update(*article)
	return tools.ResultSuccess(nil)
}

func (ac *ArticleController) PostAuthorTop() mvc.Result {
	userIdF:= ac.Ctx.FormValue("userId")
	if len(userIdF) == 0{
		return tools.ResultFailMsg("userId 参数为空")
	}
	userId,_ := strconv.ParseInt(userIdF,10,64)
	sizeF:= ac.Ctx.FormValue("size")
	size,_ := strconv.Atoi(sizeF)
	articleList := ac.ArticleService.FindAuthorTop(userId,size)
	return tools.ResultSuccess(articleList)
}
